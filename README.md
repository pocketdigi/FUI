#FUI
开源Android Flat UI库，整合各个常用UI控件,原设计图来自网络，
PSD源文件下载地址:http://www.psdoc.org/ch/document/light_color_flat_ui_kit_input.html
#包含控件：
TextView,EditText,Spinner,ProgressBar,RadioButton,CheckBox,ToggleButton,Buttun,AutoCompleteTextView,HeadMenu,SearchBar,TabBar,Indicator.
#使用方法：
将FUI项目作为library引入你的项目中,修改您的项目的AndroidManifest.xml文件，在application标签中,添加android:theme="@style/FTheme",
即可使项目中的标准Android控件变成Flat UI风格,具体可参考Demo.
#官方网站
http://www.pocketdigi.com
#2014.02.12
新增TabBar以及44个图标,规则res下文件名，避免冲突
#2013.12.08
1、增加自定义的AlertDialog
2、修复Demo中包名错误的Bug

#2013年12月4日：
第一个版本。
