package com.pocketdigi.fuidemo.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.pocketdigi.fuidemo.R;

public class ListFragment extends android.support.v4.app.ListFragment {
	ArrayAdapter<String> adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO 自动生成的方法存根
		super.onCreate(savedInstanceState);
		ArrayList<String> strs=new ArrayList<String>();
		strs.add("aaa");
		strs.add("bbb");
		strs.add("ccc");
		strs.add("ddd");
		adapter=new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_expandable_list_item_1, android.R.id.text1, strs);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO 自动生成的方法存根
		return inflater.inflate(R.layout.fragment_list, container, false);
		
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO 自动生成的方法存根
		super.onViewCreated(view, savedInstanceState);
		setListAdapter(adapter);
	}
}
