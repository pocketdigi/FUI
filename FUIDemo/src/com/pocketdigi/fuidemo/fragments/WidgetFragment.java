package com.pocketdigi.fuidemo.fragments;

import java.util.ArrayList;
import java.util.Arrays;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;

import com.pocketdigi.fui.adapters.FSpinnerAdapter;
import com.pocketdigi.fui.views.FAlertDialog;
import com.pocketdigi.fuidemo.R;

public class WidgetFragment extends Fragment implements OnItemSelectedListener{
	Spinner spinner;
	AutoCompleteTextView actv;
	FSpinnerAdapter spinnerAdapter;
	Button btn_dialog;
	ArrayList<String> list = new ArrayList<String>();
	ArrayAdapter<String> listViewAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO 自动生成的方法存根
		View v=inflater.inflate(R.layout.fragment_widget, container, false);
		spinner = (Spinner) v.findViewById(R.id.spinner);
		actv = (AutoCompleteTextView) v.findViewById(R.id.actv);
		btn_dialog = (Button) v.findViewById(R.id.btn_dialog);
		
		String[] ls = getResources().getStringArray(R.array.stringarray);
		spinnerAdapter = new FSpinnerAdapter(this.getActivity(), Arrays.asList(ls));
		spinner.setAdapter(spinnerAdapter);
		spinner.setOnItemSelectedListener(this);		
		return v;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO 自动生成的方法存根
		super.onViewCreated(view, savedInstanceState);
		btn_dialog.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO 自动生成的方法存根
				FAlertDialog dialog = new FAlertDialog(WidgetFragment.this.getActivity());
				dialog.setTitle("标题");
				dialog.setMessage("信息内容");
				dialog.setPositiveButton("确定", new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO 自动生成的方法存根
					}
				});
				dialog.setNegativeButton("取消", new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO 自动生成的方法存根
					}
				});
				dialog.show();

			}
		});
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		// TODO 自动生成的方法存根
		spinnerAdapter.setPosition(position);
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO 自动生成的方法存根
		
	}
}
