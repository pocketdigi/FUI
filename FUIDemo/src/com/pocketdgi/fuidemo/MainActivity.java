package com.pocketdgi.fuidemo;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.pocketdigi.fui.views.HeaderMenu;
import com.pocketdigi.fui.views.HeaderMenu.OnButtonClickListener;
import com.pocketdigi.fui.views.LineIndicator;
import com.pocketdigi.fui.views.TabBar;
import com.pocketdigi.fui.views.TabBar.OnTabChangeListener;
import com.pocketdigi.fuidemo.R;
import com.pocketdigi.fuidemo.fragments.ListFragment;
import com.pocketdigi.fuidemo.fragments.WidgetFragment;

public class MainActivity extends FragmentActivity implements OnTabChangeListener {
	HeaderMenu headerMenu;
	TabBar tabBar;
	FragmentManager fmger;
	WidgetFragment widgetFragment;
	ListFragment listFragment;
	LineIndicator indicator;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		headerMenu = (HeaderMenu) findViewById(R.id.headerMenu);
		tabBar=(TabBar)findViewById(R.id.tabBar);
		indicator=(LineIndicator)findViewById(R.id.indicator);
		indicator.setTabBar(tabBar);
		tabBar.addOnTabChangeListener(this);

		headerMenu.setOnButtonClickListener(new OnButtonClickListener() {

			@Override
			public void onRightClicked() {
				// TODO Auto-generated method stub
				Toast.makeText(MainActivity.this, "Right Click", Toast.LENGTH_LONG).show();
			}

			@Override
			public void onLeftClicked() {
				// TODO Auto-generated method stub
				Toast.makeText(MainActivity.this, "Left Click", Toast.LENGTH_LONG).show();
			}
		});
		
		fmger=this.getSupportFragmentManager();
		FragmentTransaction transaction=fmger.beginTransaction();
		widgetFragment=new WidgetFragment();
		transaction.add(R.id.layout_fragment_container, widgetFragment);
		transaction.commit();
		
	}
	/**
	 * 底部tab改变
	 */
	@Override
	public void onTabChanged(int tabId,int checkedIndex) {
		FragmentTransaction transaction=fmger.beginTransaction();
		switch(tabId)
		{
		case R.id.home:
			transaction.replace(R.id.layout_fragment_container, widgetFragment);
			transaction.commit();
			break;
		case R.id.setting:
			if(listFragment==null)
			{
				listFragment=new ListFragment();
			}
			transaction.replace(R.id.layout_fragment_container, listFragment);
			transaction.commit();
			break;
		case R.id.feedback:
			break;
		case R.id.exit:
			Toast.makeText(this, "Category", Toast.LENGTH_LONG).show();
			break;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO 自动生成的方法存根
		super.onDestroy();
		tabBar.destory();
	}
}
